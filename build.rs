//!
//! Cargo build wrapper to build libbitcoinconsensus
//!
//! Currently doesn't do much error checking; it is useful for quick setup
//! but on failures, you probably want to revert manual building
//!
use std::env;
use std::fs;
use std::path::PathBuf;
use std::process::Command;


fn main() {
    let src = env::current_dir().unwrap();
    let dst = PathBuf::from(env::var_os("OUT_DIR").unwrap());

    println!("cwd={:?}", src);
    println!("Build result {}", Command::new("bash").arg("build.sh").status().unwrap());

    for f in fs::read_dir("bitcoinxt/src/.libs").unwrap() {
        let f = f.unwrap();
        println!("Moving {:?}", f);
        fs::copy(&f.path(), dst.join(f.file_name())).unwrap();
    }
    for f in fs::read_dir("bitcoinxt/src/secp256k1/.libs").unwrap() {
        let f = f.unwrap();
        println!("Moving {:?}", f);
        fs::copy(&f.path(), dst.join(f.file_name())).unwrap();
    }

    println!("Build done");
    println!("cargo:rustc-link-search=native={}", env::var("OUT_DIR").unwrap());
    println!("cargo:rustc-link-lib=static=bitcoinconsensus");
    println!("cargo:rustc-link-lib=static=secp256k1");
    println!("cargo:rustc-link-search=/usr/lib/");
    println!("cargo:rustc-link-lib=boost_system");
    println!("cargo:rustc-flags=-l dylib=stdc++");
}
