
//! Rust interface to C-bitcoinconsensus lib for script validation
//!
//! The interface translates a pre-emptive flags to resulting flags; thus uses the logic
//!
//! validate() => valid_with_flags
//!
//! instead of
//!
//! validate(flags) => yes|no
//!



extern crate libc;

use std::sync::atomic;

extern {

    /// From:
    /// int bitcoinconsensus_verify_script_with_amount(const unsigned char *scriptPubKey, unsigned int scriptPubKeyLen, int64_t amount,
    ///                                    const unsigned char *txTo        , unsigned int txToLen,
    ///                                    unsigned int nIn, unsigned int flags, bitcoinconsensus_error* err)
    fn bitcoinconsensus_verify_script_with_amount(
        prevout_script:      *const u8,
        prevout_script_size: u32,
        amount:              u64,
        transaction:         *const u8,
        transaction_size:    u32,
        tx_input_index:      u32,
        flags:               u32,
        err:                 *mut i32
    )
        -> i32;
}

// typedef enum bitcoinconsensus_error_t
// {
//     bitcoinconsensus_ERR_OK = 0,
//     bitcoinconsensus_ERR_TX_INDEX,
//     bitcoinconsensus_ERR_TX_SIZE_MISMATCH,
//     bitcoinconsensus_ERR_TX_DESERIALIZE,
//     bitcoinconsensus_ERR_AMOUNT_REQUIRED,
//     bitcoinconsensus_ERR_INVALID_FLAGS,
// } bitcoinconsensus_error;

#[derive(Debug)]
pub enum VerifyScriptError {
    Index,
    SizeMismatch,
    Deserialize,
    AmountRequired,
    InvalidFlags,
}


// If we try validating with fork_id incorrectly we have to validate twice
// So we maintain a global to hint which to try first
static HINT_USE_FORK_ID: atomic::AtomicBool = atomic::AtomicBool::new(false);

// Wrapper around script verification flags
#[derive(Debug,Clone,Copy,PartialEq,Eq)]
pub struct VerifyFlags(u32);


impl VerifyFlags {
    pub const VERIFY_NONE: Self                = VerifyFlags(0);
    pub const VERIFY_P2SH: Self                = VerifyFlags(1 << 0);
    pub const VERIFY_DERSIG: Self              = VerifyFlags(1 << 2);
    pub const VERIFY_NULLDUMMY: Self           = VerifyFlags(1 << 4);
    pub const VERIFY_CHECKLOCKTIMEVERIFY: Self = VerifyFlags(1 << 9);
    pub const VERIFY_CHECKSEQUENCEVERIFY: Self = VerifyFlags(1 << 10);

    pub const ENABLE_SIGHASH_FORKID: Self      = VerifyFlags(1 << 16);

    // We add the inverse of the enale-sighash to ensure block validation can
    // be checked by ANDing the verify flags of individual transactions
    pub const NOT_ENABLE_SIGHASH_FORKID: Self      = VerifyFlags(1 << 17);

    // Used interally to indicate any validation passed
    pub const VERIFY_VALID: Self               = VerifyFlags(1<<1);

    pub fn raw(self) -> u32 { self.0 }
    pub fn from_raw(n: u32) -> Self { VerifyFlags(n) }

    pub fn invalid() -> Self { VerifyFlags(0) }

    pub fn verify_max() -> VerifyFlags { VerifyFlags(u32::max_value()) }

    /// Returns the sequece of flag-combinations that we want to check
    pub fn check_sequence(hint_use_fork_id: bool) -> [VerifyFlags; 8] {
        let mut res = [VerifyFlags::VERIFY_NONE; 8];
        let n = if hint_use_fork_id { 1 } else { 0 };
        res[5+n] = Self::VERIFY_P2SH;
        res[4+n] = res[5+n] | Self::VERIFY_DERSIG;
        res[3+n] = res[4+n] | Self::VERIFY_NULLDUMMY;
        res[2+n] = res[3+n] | Self::VERIFY_CHECKLOCKTIMEVERIFY;
        res[1+n] = res[2+n] | Self::VERIFY_CHECKSEQUENCEVERIFY;
        res[0+n] = res[1+n] | Self::VERIFY_NULLDUMMY;
        res[if hint_use_fork_id { 0 } else {7 }] = res[0+n] | Self::ENABLE_SIGHASH_FORKID;
        res
    }

    pub fn contains(self, other: VerifyFlags) -> bool {
        (self.0 & other.0) == other.0
    }
}

impl ::std::ops::BitOr for VerifyFlags {
    type Output = Self;

    fn bitor(self, rhs: Self) -> Self {
        VerifyFlags(self.0 | rhs.0)
    }
}
impl ::std::ops::BitAnd for VerifyFlags {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Self {
        VerifyFlags(self.0 & rhs.0)
    }
}


/*
pub mod ScriptFlags {
    pub const VERIFY_NONE: u32 = 0;

    // These rule changes have been activated sequentially so we only need upto specific versions
    pub const UPTO_P2SH: u32 = VERIFY_P2SH;
    pub const UPTO_DERSIG: u32 = UPTO_P2SH | VERIFY_DERSIG;
    pub const UPTO_NULLDUMMY: u32 = UPTO_DERSIG | VERIFY_NULLDUMMY;
    pub const UPTO_CHECKLOCKTIMEVERIFY: u32 = UPTO_NULLDUMMY | VERIFY_CHECKLOCKTIMEVERIFY;
    pub const UPTO_CHECKSEQUENCEVERIFY: u32 = UPTO_CHECKLOCKTIMEVERIFY | VERIFY_CHECKSEQUENCEVERIFY;
    pub const UPTO_FORKID: u32 = UPTO_CHECKSEQUENCEVERIFY | SCRIPT_ENABLE_SIGHASH_FORKID;
}
*/



/// Verifies whether the given `input` of the transaction spends the given `output`
/// using libbitcoin-consensus
pub fn verify_script(previous_tx_out: &[u8], amount: u64, transaction: &[u8], input: u32) -> Result<VerifyFlags, VerifyScriptError> {

    // The sequence with which we try flags
    let try_forkid_first = HINT_USE_FORK_ID.load(atomic::Ordering::Relaxed);

    // Try iteratively "easier" validation
    for flags in VerifyFlags::check_sequence(try_forkid_first).into_iter() {
        let mut err: i32 = 0;
        let result = unsafe {
            bitcoinconsensus_verify_script_with_amount(
                previous_tx_out.as_ptr(),
                previous_tx_out.len() as u32,
                amount,
                transaction.as_ptr(),
                transaction.len() as u32,
                input as u32,
                flags.raw(),
                &mut err
            )
        };
        if result == 1 {
            // script OK

            // save forkid-hint for next time
            let forkid = flags.contains(VerifyFlags::ENABLE_SIGHASH_FORKID);
            if forkid != try_forkid_first {
                HINT_USE_FORK_ID.store(forkid, atomic::Ordering::Relaxed);
            }
            // add inverse of forkid
            let flags = if forkid { *flags } else { *flags | VerifyFlags::NOT_ENABLE_SIGHASH_FORKID };

            return Ok(flags | VerifyFlags::VERIFY_VALID);
        }
        else if err > 0 {
            // parameter error; caller can panic
            return Err(match err {
                1 => VerifyScriptError::Index,
                2 => VerifyScriptError::SizeMismatch,
                3 => VerifyScriptError::Deserialize,
                4 => VerifyScriptError::AmountRequired,
                5 => VerifyScriptError::InvalidFlags,
                _ => unreachable!()
            })
        }
    }

    // invalid
    Ok(VerifyFlags::invalid())
}



#[cfg(test)]
mod tests {
    use super::*;

    // simple test only tests interface; actual script tests are in c++ code

    #[test]
    fn test_true_script() {
        // tx with PUSHDATA(12) as scriptsig
        let tx = &[
            3u8, 0u8, 0u8, 0u8, // version
            1u8, // input count
            0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
            0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
            0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8,
            0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, // prev_out
            0u8, 0u8, 0u8, 0u8, // prev_out_idx
            2u8, // scriplen
            1u8, 12u8, // PUSHDATA(12)
            0xFFu8, 0xFFu8, 0xFFu8, 0xFFu8,
            1u8, // output count
            100u8,0u8,0u8,0u8,0u8,0u8,0u8,0u8,
            1u8, // pkscriptlen
            0u8, //pkscript doesn't matter
            0u8, 0u8, 0u8, 0u8 // locktime
        ];
        assert!(verify_script(&[1u8,12u8,0x87u8] , 0, tx, 0).unwrap().is_some());
        assert!(verify_script(&[1u8,11u8, 0x87u8], 0, tx, 0).unwrap().is_none());
    }
}

