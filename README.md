# bitcoinconsensus-sys

bitconconsensus-sys is a rust wrapper around the verifyscript function of libbitcoinconsensus which is the library inclused in most Bitcoin/Bitcoin Cash C++ implemenetations . 

It currently uses BitcoinXT and includes it as a submodule.

## Build instruction

Install cargo nighly:

    $ curl -s https://static.rust-lang.org/rustup.sh | sh -s -- --channel=nightly

Install build dependencies for libbitconsensus. Details can be found in [bitcoinxt/doc/build-unix.md](bitcoinxt/doc/build-unix.md), or on (Debian/Ubuntu) you can issue:

    $ sudo apt-get install build-essential libtool autotools-dev autoconf pkg-config libssl-dev libcurl4-gnutls-dev libevent-dev libboost-all-dev

Build the library:

    $ cargo build --release


