#!/bin/bash

# Bare build script
# Assumes dependencies are installed
pushd bitcoinxt
./autogen.sh
./configure \
  --disable-maintainer-mode \
  --disable-dependency-tracking \
  --disable-wallet \
  --disable-tests \
  --disable-bench \
  --without-miniupnpc \
  --without-qrencode \
  --without-utils \
  --without-daemon \
  --with-gui=no

make

popd
